export const redirectURL = process.env.REACT_APP_REDIRECT_URL;

export const limitQueryPlaylists = 20;

const scopes = [
  "app-remote-control",
  "streaming",
  "ugc-image-upload",
  "user-read-currently-playing",
  "user-read-recently-played",
  "user-read-playback-state",
  "user-read-playback-position",
  "user-read-email",
  "user-read-private",
  "user-top-read",
  "user-modify-playback-state",
  "user-library-read",
  "user-library-modify",
  "user-follow-read",
  "user-follow-modify",
  "playlist-modify-private",
  "playlist-modify-public",
  "playlist-read-private",
  "playlist-read-collaborative"
];

export const loginURL = `https://accounts.spotify.com/authorize?client_id=${ process.env.REACT_APP_CLIENT_ID }
&response_type=code&redirect_uri=${ redirectURL }&scope=${ scopes.join("%20") }
&show_dialog=false`;
