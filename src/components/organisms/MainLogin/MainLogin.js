import React from 'react'
import { FormMain } from '../../molecules/FormMain/FormMain'

export const MainLogin = () => {
  return (
    <div className='o-form-container'>
      <div className='o-form__ball ball1'></div>
      <div className='o-form__ball ball2'></div>
      <div className='o-form__ball ball3'></div>
      <FormMain />
    </div>
  )
}
