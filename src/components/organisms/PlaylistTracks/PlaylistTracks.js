import React from 'react'
import { useSelector } from 'react-redux';
import { Track } from '../../atoms/Track/Track';
import { HeaderPlaylistTrack } from '../../molecules/HeaderPlaylistTrack/HeaderPlaylistTrack';

export const PlaylistTracks = ({ favorite }) => {

  const { activePlaylist, favorites } = useSelector(state => state.tracks)

  const { items } = favorite ? favorites : activePlaylist || []

  return (
    <div className='o-playlist-tracks-container'>

      <HeaderPlaylistTrack />
      
      {
        items?.map( (track, index) => <Track key={ index } item={{...track, number: index+1}} favoritePage={favorite}/>)
      }
    </div>
  )
}
