import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom'


export const TabMenu = ({ title, icon, route }) => {
  return (
    <Link className='m-tab-menu-container' to={ route }>
      <FontAwesomeIcon  icon={icon} /><p>{ title }</p>
    </Link>
  )
}
