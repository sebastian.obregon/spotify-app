import { faHeart as faHeartSolid } from '@fortawesome/free-solid-svg-icons'
import { faHeart as faHeartOutline } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { msToMinutes } from '../../../shared/convertMSToMinutes'
import { trimText } from '../../../shared/trimText'
import { useDispatch } from 'react-redux'
import { addTrackFavorite, removeTrackFavorite } from '../../../redux/actions/tracks'

export const Track = ({ item, favoritePage }) => {

  const dispatch = useDispatch()

  const { 
    id,
    name,
    number,
    artists,
    album,
    duration_ms,
    favorite
  } = item

  const singer = artists[0]?.name
  const image = album?.images[0].url || album?.images[1].url
  const albums = album?.name
  const release_date = album?.release_date

  const handlerClickFavorite = async() => {

    favorite ?  dispatch( await removeTrackFavorite( id, favoritePage ) ) : await dispatch( addTrackFavorite( id, favoritePage) )

  }

  return (
    <div className='a-track-container'>
      
      <span className='a-track__number'>{ number }</span>

      <div className='a-track__content'>
        <img className='a-track__img' src={ image || '' } alt='copy track'/>
        <div className='a-track-content__info'>
          <h4 className='a-track__name'>{ trimText( name, 50) }</h4>
          <p className='a-track__singer'>{ trimText( singer ) }</p>
      </div>
      </div>

      <p className='a-track__album--hidden'>{ albums }</p>
      <p className='a-track__album--hidden'>{ release_date }</p>

      <div className='a-track__time'>
        <span className='a-track__favorite' onClick={ handlerClickFavorite }>
          <FontAwesomeIcon icon={ favorite ? faHeartSolid : faHeartOutline} size='1x' color={ favorite ? '#B960FF' : '#FFF' }/>
        </span>
        <p className='a-track__duration'>{ msToMinutes( duration_ms ) }</p>
      </div>

    </div>
  )
}
