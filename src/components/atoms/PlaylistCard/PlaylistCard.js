import { faHeart as faHeartSolid } from '@fortawesome/free-solid-svg-icons'
import { faHeart as faHeartOutline } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'
import { trimText } from '../../../shared/trimText'
import { Link } from 'react-router-dom'

export const PlaylistCard = ( { playlist } ) => {

  const { id, name, description, images, favorite } = playlist
  const image = images[0]?.url;

  return (
    <Link className='a-playlist-card-container' to={`/playlist/${ id }`}>
      
      <img className='a-card-playlist__img' alt={`${ name }`} src={ image }/>

      <div className='text-wrapper'>
        <h4 className='text'>{ trimText( name, 30 ) }</h4>
        <p className='text'>{ trimText( description ) }</p>
      </div>

      <div className='a-card-favorite'>
        <FontAwesomeIcon icon={ favorite ? faHeartSolid : faHeartOutline} size='1x'/>
      </div>
    </Link>
  )
}
