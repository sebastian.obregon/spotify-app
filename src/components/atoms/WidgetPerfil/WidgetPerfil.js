import { faArrowAltCircleDown, faArrowAltCircleUp, faUser } from '@fortawesome/free-regular-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { startLogout } from '../../../redux/actions/auth'
import { getName } from '../../../shared/getName'

export const WidgetPerfil = () => {

  const { user } = useSelector(state => state.auth)
  const dispatch = useDispatch()
  const [modal, setModal] = useState(true)

  const handerLogout = () => {
    window.localStorage.removeItem('token_access')
    dispatch( startLogout() )
  }

  const handlerMenu = () => {
    setModal(!modal)
    document.querySelector('.a-items-container').style.display = modal ? 'block': 'none'
  }

  return (
    <div className='a-home-perfil-container' onClick={ handlerMenu }>
      <div className='a-home-perfil'>
        <img className='a-perfil__img' src={ user.image } alt='Imagen perfil'></img>

        <p className='a-perfil__name'>{ getName( user.display_name ) }</p>

        <FontAwesomeIcon className='a-perfil__arrow'  icon={ modal ? faArrowAltCircleDown : faArrowAltCircleUp } />

        <div className='a-items-container'>
          <ul>
            <li onClick={ handerLogout }><FontAwesomeIcon  icon={ faUser } /><p>Cerrar sesión</p></li>
          </ul>
        </div>
      </div>
    </div>
  )
}
