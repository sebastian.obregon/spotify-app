import React from 'react'
import { loginURL } from '../../../services/Spotify/spotifyServices'

export const FormMain = () => {

  const handleLogin = (e) => {
    e.preventDefault();
    window.location.href = loginURL;

  }


  return (
      <form className='container__formulario' onSubmit={handleLogin}>
        <h3 className='formulario__title'>Iniciar sesión</h3>

        <div className='formulario__group form-margin'>
          <input className='input-main' placeholder='Correo electrónico' type='email'/>
          <input className='input-main' placeholder='Contraseña' type='password'/>
        </div>
        
        <div className='formulario__group'>
          <button className='btn btn-primary'>INGRESAR</button>
          <button className='btn btn-outline'>INICIAR SESIÓN CON SPOTIFY</button>
        </div>
      </form>
  )
}
