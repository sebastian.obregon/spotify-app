
import { useSelector } from 'react-redux'
import { PlaylistCard } from '../../atoms/PlaylistCard/PlaylistCard'

export const PlaylistGrid = () => {

  const { playlists } = useSelector(state => state.tracks)
  
  return (
    <div className='m-playlist-container'>
      {
        playlists?.map( (playlist, index) => {
          return <PlaylistCard key={ index } playlist={ playlist }/>
          
        })
      }       
    </div>
  )
}
