import React from 'react'

export const HeaderPlaylistTrack = () => {
  return (
    <>
      <div className='m-playlist__header'>
        <h4 className='m-header__number m-header'>#</h4>
        <h4 className='m-header__title m-header'>TITULO</h4>
        <h4 className='m-header__item--hidden m-header'>ÁLBUM</h4>
        <h4 className='m-header__item--hidden m-header'>LANZAMIENTO</h4>
        <h4 className='m-header__duration m-header'>DURACIÓN</h4>
      </div>
      <hr></hr>
    </>
  )
}
