import React from 'react'
import logo from '../../../assets/logo.png'
import FontAwesome from 'react-fontawesome';
import { useState } from 'react';
import { Link } from 'react-router-dom'

export const NavbarOut = () => {

  const [open, setOpen] = useState(false)


  const handleMenu = () => {
    setOpen(!open);
  }

  return (
    <div className="m-nav-container">
      
      <Link to='/'><img src={ logo } alt='logo' className='m-nav__img'/></Link>

      <nav className={`m-nav__options ${open ? 'open' : ''}`}>
        <ul className='m-nav__list'>
          <li className='m-nav__item'><a className='m-nav__options--option' href={ process.env.REACT_APP_REDIRECT_URL }>Registrarse</a></li>
          <li className='m-nav__item'><a className='m-nav__options--option' href={ process.env.REACT_APP_REDIRECT_URL }>Iniciar sesión</a></li>
        </ul>
      </nav>

      <div className='m-nav-hamburger' onClick={ handleMenu }>
          <FontAwesome 
            name={!open ? 'bars' : 'times'}
            size='2x'
          />
      </div>
    </div>
  )
}
