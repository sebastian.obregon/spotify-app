import React, { useState } from 'react'
import logo from '../../../assets/logo.png'
import { TabMenu } from '../../atoms/TabMenu/TabMenu'
import { faHome } from '@fortawesome/free-solid-svg-icons'
import { faHeart } from '@fortawesome/free-regular-svg-icons'
import FontAwesome from 'react-fontawesome'
import { Link } from 'react-router-dom'

export const SideMenu = () => {

  const [open, setOpen] = useState(false)

  const handleMenu = () => {
    setOpen(!open);
  }


  return (
    <div className='m-menu-container'>

      <Link to='/'><img src={ logo } alt='logo' className='m-nav__img'/></Link>


      <nav className={`m-menu-nav ${open ? 'open' : ''}`}>
        <ul className='m-menu-nav__list'>
          <TabMenu title='Home' icon={ faHome } route='/'/>
          <TabMenu title='Favoritos' icon={ faHeart } route='/favorites'/>
        </ul>
      </nav>


      <div className={`menu-icon ${open ? 'static-menu' : ''}`} onClick={ handleMenu }>
          <FontAwesome
            name={!open ? 'bars' : 'times'}
            size='2x'
          />
      </div>
    </div>
  )
}
