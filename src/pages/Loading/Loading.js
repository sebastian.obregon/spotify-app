import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React from 'react'

export const Loading = () => {
  return (
    <div className='p-loading-container'>
      <FontAwesomeIcon className='p-loading__img' icon={ faSpinner } size='5x' spin></FontAwesomeIcon>
      <p>Cargando</p>
    </div>
  )
}
