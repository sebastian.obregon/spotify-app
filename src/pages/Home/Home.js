import React, { useEffect, useState } from 'react'
import { HomeMenu } from '../../templates/HomeMenu/HomeMenu'
import { PlaylistGrid } from '../../components/molecules/PlaylistGrid/PlaylistGrid'
import { useDispatch, useSelector } from 'react-redux'
import { playlistsStartLoading } from '../../redux/actions/tracks'
import { Loading } from '../Loading/Loading'
import { Waypoint } from 'react-waypoint'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { limitQueryPlaylists } from '../../services/Spotify/spotifyServices'

export const Home = () => {

  const dispatch = useDispatch();
  const { playlists, nextPlaylists } = useSelector(state => state.tracks)
  const [offset, setOffset] = useState(limitQueryPlaylists)

  useEffect(() => {
    dispatch( playlistsStartLoading() )
  }, [dispatch])

  const handlerLoadMore = () => {
    if( nextPlaylists ) {
      setOffset( offset + limitQueryPlaylists )
      dispatch( playlistsStartLoading( true, offset) )
    }
  }

  if ( !playlists ) return <Loading/>

  return (
    <HomeMenu>
      <h2 className='p-home-title'>Tus playlists favoritas</h2>

      <PlaylistGrid />

      {
        nextPlaylists &&       
          <Waypoint onEnter={ handlerLoadMore }>
            <p className='p-home__loader'>Cargando <FontAwesomeIcon icon={ faSpinner } spin/></p>
          </Waypoint>
      }

    </HomeMenu>
  )
}
