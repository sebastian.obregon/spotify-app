import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { useParams } from 'react-router'
import { getPlaylistById } from '../../redux/actions/tracks'
import { HomeMenu } from '../../templates/HomeMenu/HomeMenu'
import { Playlist } from '../../templates/Playlist/Playlist'
import { Loading } from '../Loading/Loading'

export const PlaylistMain = () => {

  const { playlistId } = useParams()
  const dispatch = useDispatch()
  const { activePlaylist } = useSelector(state => state.tracks)
  const { name, images } = activePlaylist || ''
  const image = !!images && images[0].url;


  useEffect(() => {
    dispatch( getPlaylistById( playlistId ) )
  }, [ dispatch, playlistId ])


  if ( !activePlaylist  ) return <Loading/>

  return (
     <HomeMenu>

        <Playlist title={ name } image={ image }></Playlist>

    </HomeMenu>

  )
}
