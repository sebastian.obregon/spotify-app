import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { Waypoint } from 'react-waypoint'
import { getPlaylistFavorites } from '../../redux/actions/tracks'
import { limitQueryPlaylists } from '../../services/Spotify/spotifyServices'
import { HomeMenu } from '../../templates/HomeMenu/HomeMenu'
import { Playlist } from '../../templates/Playlist/Playlist'
import { Loading } from '../Loading/Loading'

export const Favorites = () => {

  const dispatch = useDispatch()
  const { favorites, nextFavorites } = useSelector(state => state.tracks)
  const [offset, setOffset] = useState(limitQueryPlaylists)
  
  useEffect(() => {
    dispatch( getPlaylistFavorites() )
  }, [dispatch])

  
  const handlerLoadMore = () => {
    if( nextFavorites ) {
      setOffset( offset + limitQueryPlaylists )
      dispatch( getPlaylistFavorites( true, offset) )
    }
  }
  
  if ( !favorites ) return <Loading/>

  return (
    <HomeMenu>
        <Playlist title='Tus favoritos' image='https://t.scdn.co/images/3099b3803ad9496896c43f22fe9be8c4.png' favorite={ true }></Playlist>

        {
          nextFavorites &&       
          <Waypoint onEnter={ handlerLoadMore }>
            <p className='p-home__loader'>Cargando <FontAwesomeIcon icon={ faSpinner } spin/></p>
          </Waypoint>
        }
    </HomeMenu>
  )
}

