import React from 'react'
import { NavbarOut } from '../../components/molecules/NavbarOut/NavbarOut'
import { MainLogin } from '../../components/organisms/MainLogin/MainLogin'

export const Login = () => {
  return (
    <div className='p-login-container'>
      <NavbarOut />

      <MainLogin/>
    </div>
  )
}
