import React from 'react'
import { PlaylistTracks } from '../../components/organisms/PlaylistTracks/PlaylistTracks'

export const Playlist = ({ title, image, favorite=false }) => {
  return (
    <div className='t-playlist-container'>
      
      <div className='t-playlist__header'>
        <img className='t-playlist__cover' alt='imagen playlist' src={ image ? image : '' }/>
        <h2 className='t-playlist__title'>{ title }</h2>
      </div>

      <PlaylistTracks favorite={favorite}/>


    </div>
  )
}
