import React from 'react'
import { SideMenu } from '../../components/molecules/SideMenu/SideMenu'
import { WidgetPerfil } from '../../components/atoms/WidgetPerfil/WidgetPerfil'

export const HomeMenu = ( {children} ) => {

  return (
    <div className='t-home-container'>
      <SideMenu />
      
      <div className='t-home-content'>
        {children}
      </div>

      <WidgetPerfil />

    </div>
  )
}
