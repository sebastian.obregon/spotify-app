import { types } from "../types/types";


const initialState = {
  playlists: null,
  activePlaylist: null,
  favorites: null,
  nextPlaylists: null,
  nextFavorites: null
/*   uid: null,
  name: null */
}

export const tracksReducer = ( state = initialState, action ) => {

  switch (action.type) {

    case types.playlistLoaded:
      return {
        ...state,
        playlists: [...action.payload.items],
        nextPlaylists: action.payload.next
      };

    case types.playlistNewLoaded:
      return {
        ...state,
        playlists: [...state.playlists, ...action.payload.items],
        nextPlaylists: action.payload.next
      };

    case types.tracksPlaylistLoaded:
      return {
        ...state,
        activePlaylist: {...action.payload}
      };

    case types.favoritesLoaded:
      return {
        ...state,
        favorites: {...action.payload.items},
        nextFavorites: action.payload.next
      };

    case types.favoritesNewLoaded:
      return {
        ...state,
        favorites: { items: [...state.favorites.items, ...action.payload.items.items]},
        nextFavorites: action.payload.next
      };

    case types.tracksLogout:
      return {
        ...initialState
      };

    case types.activePlaylistRemove:
      return {
        ...state,
        activePlaylist: null
      };
  
    default:
      return state;
  }

}