import { combineReducers } from "redux";
import { authReducer } from "./authReducer";
import { tracksReducer } from "./tracksReducer";
import { uiReducer } from "./uiReducer";


export const rootReducer = combineReducers({
  ui: uiReducer,
  auth: authReducer,
  tracks: tracksReducer
})