import { types } from "../types/types";


const initialState = {
  access_token: null,
  user: {}
/*   uid: null,
  name: null */
}

export const authReducer = ( state = initialState, action ) => {

  switch (action.type) {

    case types.authLogin:
      return {
        ...state,
        access_token: action.payload
      };

    case types.userProfileLoaded:
      return {
        ...state,
        user: action.payload
      };

    case types.expiredToken:
      return {
        ...state,
        access_token: null
      };

    case types.authLogout:
      return {
        ...initialState
    };
  
    default:
      return state;
  }

}
