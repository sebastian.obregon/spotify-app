import { fetchAuth, fetchSpotify } from "../../shared/fetch";
import { types } from "../types/types";
import { tracksLogout } from "./tracks";


export const login = ( access_token ) => ({
  type: types.authLogin,
  payload: access_token
})

export const startLogout = () => {
  return ( dispatch ) => {

    window.localStorage.clear();
    dispatch( tracksLogout() );
    dispatch( logout() );
  }
}

const logout = () => ({
  type: types.authLogout
})

//Consultar acces y refresh token
export const getAuthToken = ( refresh=false) => {
  return async ( dispatch ) => {

    try {

      const resp = await fetchAuth( 'token', 'POST', refresh );
      const body = await resp.json();

      const { access_token, expires_in, token_type } = body;

      window.localStorage.setItem('access_token', access_token)
      !refresh && window.localStorage.setItem('refresh_token', body.refresh_token)
      window.localStorage.setItem('expires_in', expires_in)
      window.localStorage.setItem('token_type', token_type)

     
      dispatch( login( access_token ) )
      dispatch( getUserProfile() )



    } catch (error) {
      console.log(error)
    }

  }
}

//Consultar información del usuario
export const getUserProfile = () => {
  return async ( dispatch ) => {

    try {

      const resp = await fetchSpotify( 'me' );
      const body = await resp.json();

      const { display_name, email, id, images } = body;

      let json = { display_name, email, id, image: images[0].url}

      dispatch( userProfileLoaded( json ) )
     

    } catch (error) {
      console.log(error)
    }

  }
}

export const userProfileLoaded = ( user ) => ({
  type: types.userProfileLoaded,
  payload: user
})

export const expiredToken = () => ({
  type: types.expiredToken
})

