import { limitQueryPlaylists } from "../../services/Spotify/spotifyServices";
import { fetchSpotify } from "../../shared/fetch";
import { types } from "../types/types";

//Consultar playlists del usuario
export const playlistsStartLoading = ( morePlaylists=false, offset=0, limit=limitQueryPlaylists ) => {
  return async ( dispatch ) => {

    try {

      const resp = await fetchSpotify( `me/playlists?offset=${offset}&limit=${limit}` );
      const body = await resp.json();
      const { items, next } = body
      morePlaylists ? dispatch( playlistsNewLoaded( items, !!next ) ) : dispatch( playlistsLoaded( items, !!next ) )

    } catch (error) {
      console.log(error)
    }

  }
}

//Guardar playlists consultadas en el store
const playlistsLoaded = ( playlists, next ) => ({
  type: types.playlistLoaded,
  payload: { items: playlists, next }
})

//Agregar nuevas playlists cargadas
const playlistsNewLoaded = ( playlists, next ) => ({
  type: types.playlistNewLoaded,
  payload: { items: playlists, next }
})

//Consultar canciones de una playlist especifica
export const getPlaylistById = ( playlistId, like=false ) => {
  return async ( dispatch ) => {

    !like && dispatch( activePlaylistRemove() )
    
    try {

      const resp = await fetchSpotify( `playlists/${playlistId}` );
      const body = await resp.json();

      const { id, name, images, tracks } = body;
      const { items } = tracks

      let json = { id, name, images, items: await queryFavoriteTracks( items )}

      dispatch( playlistLoaded( json ) )
      
      
    } catch (error) {
      console.log(error)
    }
    
  }
}

//Guardar playlists consultadas en el store
const playlistLoaded = ( active ) => ({
  type: types.tracksPlaylistLoaded,
  payload: active
})

//Consultar canciones de Mis Favoritos
export const getPlaylistFavorites = ( moreTracks=false, offset=0, limit=limitQueryPlaylists) => {
  return async ( dispatch ) => {

    try {

      const resp = await fetchSpotify( `me/tracks?offset=${offset}&limit=${limit}` );
      const body = await resp.json();

      const { items, next } = body;

      let json = { items: await queryFavoriteTracks( items ) }
      console.log('nuevos', json, items, next, moreTracks)
      moreTracks ? dispatch( favoritesNewLoaded( json, !!next ) ) : dispatch( favoritesLoaded( json, !!next ) )

    } catch (error) {
      console.log(error)
    }

  }
}

//Consulta si las canciones son favoritas o no - Max 50 por petición
const queryFavoriteTracks = async ( items=[] ) => {

  try {
    const limit = 50
    let peticiones = [];
    
    //Hacer las consultas dependiendo el máximo que es 50
    for (let i = 0; i < items.length; i += limit) {
      let pedazo = items.slice(i, i + limit);
      const cadenaIds = pedazo.reduce( (acum, item) => acum.concat(item.track.id, ','), '')
      const resp = await fetchSpotify( `me/tracks/contains?ids=${cadenaIds}` );
      const body = await resp.json();
      peticiones = [...peticiones, ...body]
    }

    return items.map((item, index) => ({...item.track, favorite: peticiones[index]}))

  } catch (error) {
    console.log(error)
  }
}

//Guardar canciones favoritas en el store
const favoritesLoaded = ( favorites, next ) => ({
  type: types.favoritesLoaded,
  payload: { items: favorites, next }
})

//Adicionar canciones favoritas en el store
const favoritesNewLoaded = ( favorites, next ) => ({
  type: types.favoritesNewLoaded,
  payload: { items: favorites, next }
})

export const tracksLogout = () => ({
  type: types.tracksLogout
})

//Quitar playlist activa
const activePlaylistRemove = () => ({
  type: types.activePlaylistRemove
})

//Agregar una canción a favoritos
export const addTrackFavorite = ( idTrack, favorite=false ) => {
  return async ( dispatch, getState ) => {

    try {
      const resp = await fetchSpotify( `me/tracks?ids=${ idTrack }`, 'PUT' );
      
      if(resp.ok) {

        if( favorite ){
          dispatch( getPlaylistFavorites() )
        }else{
          const { id } = getState().tracks.activePlaylist
          dispatch( getPlaylistById( id, true ) )
        } 

      }
    } catch (error) {
      console.log(error)
    }

  }
}

//Eliminar una canción a favoritos
export const removeTrackFavorite = ( idTrack, favorite=false ) => {
  return async ( dispatch, getState ) => {

    try {
      const resp = await fetchSpotify( `me/tracks?ids=${ idTrack }`, 'DELETE' );
      
      if(resp.ok) {

        if( favorite ){
          dispatch( getPlaylistFavorites() )
        }else{
          const { id } = getState().tracks.activePlaylist
          dispatch( getPlaylistById( id, true ) )
        } 

      }
    } catch (error) {
      console.log(error)
    }

  }
}


