export const types = {

  authLogin: '[auth] Start login',
  authLogout: '[auth] Start logout',
  userProfileLoaded: '[auth] User Profile Loaded',
  expiredToken: '[auth] Expired Token',

  startLoading: '[ui] Start loading',
  endLoading: '[ui] End loading',

  playlistLoaded: '[tracks] Playlist Loaded',
  playlistNewLoaded: '[tracks] Playlist New Loaded',
  tracksPlaylistLoaded: '[tracks] Tracks Playlist Loaded',
  favoritesLoaded: '[tracks] Favorites Loaded',
  favoritesNewLoaded: '[tracks] Favorites New Loaded',
  tracksLogout: '[tracks] Tracks Logout',
  activePlaylistRemove: '[tracks] Active Playlist Remove',

}