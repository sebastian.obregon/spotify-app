import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux';
/* import { useDispatch, useSelector } from 'react-redux'; */
import {
  BrowserRouter as Router,
  Switch,
  Redirect,
  withRouter,
} from "react-router-dom";
import { Favorites } from '../pages/Favorites/Favorites';
/* import { startChecking } from '../actions/auth'; */
import { Home } from '../pages/Home/Home';
import { Login } from '../pages/Login/Login';
import { PlaylistMain } from '../pages/PlaylistMain/PlaylistMain';
import { getAuthToken } from '../redux/actions/auth';
import { getTokenFromParams } from '../shared/getTokenHash';
import { PrivateRoute } from './PrivateRoute';
import { PublicRoute } from './PublicRoute';

export const AppRouter = () => {


  const dispatch = useDispatch();
  const { access_token } = useSelector(state => state.auth);

  useEffect(() => {
    const token = window.localStorage.getItem('token');
    if ( !token ) {

      if( window.location.search ) {

        const { code } = getTokenFromParams();
        localStorage.setItem('token', code)

        dispatch( getAuthToken() )

      }
    }else {
        dispatch( getAuthToken( true ) )
    }

  }, [ dispatch ])


  return (
    <Router>
      <div>
        <Switch>
          <PrivateRoute exact path='/' component={ withRouter(Home) } isAuthenticated={ !!access_token }/>
          <PrivateRoute exact path='/favorites' component={ withRouter( Favorites ) } isAuthenticated={ !!access_token }/>
          <PrivateRoute exact path='/playlist/:playlistId' component={ withRouter( PlaylistMain ) } isAuthenticated={ !!access_token }/>
          <PublicRoute exact path='/login' component={ withRouter(Login) } isAuthenticated={ !!access_token }/>

          <Redirect to='/'/>
        </Switch>
      </div>
    </Router>
  )
}