//Método para obtener access token del hash url
export const getTokenFromHash = () => {

  return window.location.hash
    .substring(1)
    .split('&')
    .reduce((arr, item) => {
      let param = item.split('=');

      arr[param[0]] = decodeURIComponent(param[1]);

      return arr;
    }, {});
};

//Método para obtener parametros del URL
export const getTokenFromParams = () => {

  return window.location.search
    .substring(1)
    .split('&')
    .reduce((arr, item) => {
      let param = item.split('=');

      arr[param[0]] = decodeURIComponent(param[1]);

      return arr;
    }, {});
};
