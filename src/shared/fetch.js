import { redirectURL } from "../services/Spotify/spotifyServices";
import { convertJson } from "./convertJSON";

const baseUrl = process.env.REACT_APP_API_URL;
const baseUrlAuth = process.env.REACT_APP_API_AUTH;
const clientID = process.env.REACT_APP_CLIENT_ID;
const clientSecretID = process.env.REACT_APP_CLIENT_SECRET;

const fetchSpotify = ( endpoint, method = 'GET', data='' ) => {

  const url = `${ baseUrl }/${ endpoint }`;

  const token = window.localStorage.getItem('access_token')

  if( method === 'GET' ) {

    return fetch( url, {
      method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      }
    });

  }else{

    return fetch( url, {
      method,
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
        'Authorization': `Bearer ${token}`
      },
      body: {}
    })
  }
}

const fetchAuth = ( endpoint, method = 'GET', refresh = false, data ) => {

  const url = `${ baseUrlAuth }/${ endpoint }`;

  const client = Buffer.from(`${clientID}:${clientSecretID}`).toString("base64")

  const body = !refresh 
    ? 
    
    {
      ...data,
      'grant_type': 'authorization_code',
      'code': `${window.localStorage.getItem('token') || ''}`,
      'redirect_uri': `${redirectURL}`,
    }  
    : 
    {
      ...data,
      'grant_type': 'refresh_token',
      'refresh_token': `${window.localStorage.getItem('refresh_token') || ''}`,
    }

  const final = convertJson(body)

  if( method === 'GET' ) {

    return fetch( url, {
      method,
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
        'Authorization': `Basic ${ client }`
      }
    });

  }else{

    return fetch( url, {
      method,
      headers: {
        'Content-type': 'application/x-www-form-urlencoded',
        'Authorization': `Basic ${ client }`
      },
      body: final
    })
  }
}

export {
  fetchSpotify,
  fetchAuth
}
