export const convertJson = ( obj={} ) => {
  return Object.keys(obj).reduce(function(p, c) {                         
    return p.concat([encodeURIComponent(c) + "=" + encodeURIComponent(obj[c])]);                     
  }, []).join('&');
}